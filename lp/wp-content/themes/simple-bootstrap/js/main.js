var Main = {
    selectedMenuItem: null,
    init: function () {
        // Main.homeMenuSettings();
        Main.attachEvents();
        Main.scrollToTop();
        Main.scrollToTopInit();
    },
    attachEvents: function () {


        // jQuery('ul.dropdown-menu .menu-item-has-children > a').on('click', function (event) {
        //     event.preventDefault();
        //     event.stopPropagation();
        //     jQuery(this).parent().siblings().removeClass('open');
        //     jQuery(this).parent().toggleClass('open');
        // });
    },

    scrollToTopInit: function () {
        jQuery(".scroll-top,.scroll-top-mobile").click(function () {
            jQuery("html, body").animate({
                scrollTop: 0
            }, "slow");
            return false;
        });

    },
    scrollToTop: function () {
        jQuery(window).scroll(function () {
            jQuery('.scroll-top').hide();
            if (jQuery(this).scrollTop() > 150) {
                jQuery('.scroll-top').fadeIn();
                jQuery("nav.navbar").addClass("nav-drag");
            } else {
                jQuery('.scroll-top').fadeOut();
                jQuery("nav.navbar").removeClass("nav-drag");
            }
        });
    }

};

jQuery(document).ready(function () {
    Main.init();
});