<?php
$phone = '073-708-8858';
?>
<div class="socials-bar">
    <ul>
        <li><a href="https://www.facebook.com/kenhatuki/" target="_blank"><i class="fa fa-2x fa-facebook"></i></a></li>
        <li><a href="https://www.youtube.com/channel/UCpq6j18SNYQWn9dGhbX0-EQ" target="_blank"><i class="fa fa-2x fa-youtube"></i></a></li>
        <li><a href="#" target="_blank"><i class="fa fa-2x fa-instagram"></i></a></li>
        <li><a href="tel:<?=$phone;?>" target="_blank"><i class="fa fa-2x fa-phone"></i></a></li>
        <li><a href="mailto:kenhatukii@gmail.com" target="_blank"><i class="fa fa-2x fa-envelope"></i></a></li>
    </ul>
</div>