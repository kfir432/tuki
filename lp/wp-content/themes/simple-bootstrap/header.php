<!doctype html>

<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
</head>
<?php
$phone = '073-708-8858';
?>
<body <?php body_class(); ?>>

<div id="content-wrapper">
    <div class="mobile header-call">
        <span><?php _e('Call Now', 'simple-bootstrap'); ?> - <a href="tel:<?=$phone?>"><?=$phone?></a></span>
    </div>
    <div id="page-content" class="col-xs-12">
        <div class="container">
            <header class="landing-header">

                <div class="brand">
                    <a title="<?php bloginfo('description'); ?>"
                       href="<?php echo esc_url(home_url('/')); ?>">
                        <img id="logo-flat" height="180"
                             alt="<?php bloginfo('name'); ?>"
                             src="<?php echo get_bloginfo('template_directory'); ?>/img/logo.svg"/>
                    </a>
                </div>
                <?php include_once 'socials.php';?>

            </header>
        </div>

        <div class="container-fluid">
