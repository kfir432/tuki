<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tuki_db_lp');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '* W!YN%t]MF+he<rQIpAdZRaM16,6_1s XDnRMD6ecy_xn27YE+,n/pn``I<PI{L');
define('SECURE_AUTH_KEY',  'TT+D ci|[&[7+w3vB03u3VTMShIc:j@&]Y<Ymv}CK2B@{1B)]lAv[}W<?U4ZX3t|');
define('LOGGED_IN_KEY',    '~g]=8E743H.-+, -.TF5^!fH6o[:_[d{{K2]8}bv9ou#VC 5.N-d~RIkVZhn?}Rj');
define('NONCE_KEY',        's!=VAW((]y.U:Ij*F~;T(smOXUnl$p|dE%R]_cWu=^EXKsw=ibMlcYNNn7<@iM/!');
define('AUTH_SALT',        '5CN[Vr|ZW&hf3IuQ?9X#I;bsOYCNQlpB%D~^ps{])xZE~WH|M-Rw-<Oeb28{XwP#');
define('SECURE_AUTH_SALT', ':=zps^fay*Zu7Z(4)W(^qG-u`:Nvx2N!74+1Q|n_ML<F$&2`@6_mImHvJB]]jukg');
define('LOGGED_IN_SALT',   'C{kV%~c*`uuT;6]B)s^uQyHp6buh-MJd`abMV4RuOM-Lv_%zA?$YC1(|zbo/dlw+');
define('NONCE_SALT',       'mw8no$lx%EG+n:8O)ZWSm9by/3<o`WX)1XP+,ku/<vqsbTJ$#4UgWs/}(!7|%[r|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
