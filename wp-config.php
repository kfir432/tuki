<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tuki_db');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`)xf@M(2Ef6CXo,s`whXim89l415)ShjHIfB(?-|HfISgH?f>Q8?L6Nce)N<>C.7');
define('SECURE_AUTH_KEY',  '%I`8{@25n0@0QtfHEr^HAM$a$!x}-j3$OacJU%*1NyJ6H nXtw8oh7R,dr_FHoM+');
define('LOGGED_IN_KEY',    'J*evw4b9Z!HaDEKxFW/v9Fr+`[mXHC*]S{o!X^,7:DiugeOUBjHgk*(tv;(i0 L{');
define('NONCE_KEY',        '|N^TI4Ybt:TX]C&&{/s&+3Q5#MzAu-^n9j^I0,DwL?D-4! 9%Wgi?Z(+Z`>.~,}H');
define('AUTH_SALT',        'lk#}kYFrEfftSz.2=0KL9 nQ^WJ4Z*pkY9mX=i[#N>8a(N6e^7Dct)q9J_xhIStQ');
define('SECURE_AUTH_SALT', '^5<Vq6vGAO9c86q (4.21b0^`zGvLkYc=:iG/V}~<$UbVintNVmvv!rwz$e,LLxp');
define('LOGGED_IN_SALT',   'Bdo<QHhfg )5Apx[v,8I=hJ7=oUEu-,Vr>ZKGJN3_^ctr*c/%EWV(N]!,:_q@z@H');
define('NONCE_SALT',       'c5iTLC|]%=Fo]b3K7Bnwt>%&,lgPDKgiF@dwBBC4SWgTPbMsoB#|&WXPgN3]n* l');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
